# build_envs_rw_app

This is the right way to hide api keys. main_dev, main_prod, etc are gitignored as
they have real keys. main_Ci is the only one that is not gitignored as it has no 
api keys.

Eeveryone else ran to the Googler hint in Google Crashy app demo code about using 
on constants file. But, in this case the Googler is wrong as you do not want the 
different build variants mixed and coupled together via a constants file that gets 
git ignored. Yes, even Googlers can goof up!

Templates of how the gitignored build variants should be coded i9n templates folder.

# Implementation hates

I have to init logging setup and app exception catching in the MyApp and 
no singleton. But, hey it matches the Ci server setup set of issues 
of keeping api keys out of git.

# Crdits

Code cleaded up from 

[separating build environements](https://github.com/roughike/separating_build_environments)

as he did not update to stronger type enforcement in dart 2.5

