import 'package:flutter/material.dart';
import 'package:build_envs_rw_app/app_config.dart';
import 'package:build_envs_rw_app/my_home_page.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final AppConfig config = AppConfig.of(context);

    return MaterialApp(
      title: config.appName,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}